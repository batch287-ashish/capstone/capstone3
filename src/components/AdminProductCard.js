import { Card, Button, Col, Row } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { Link, useNavigate} from 'react-router-dom';
import Swal from "sweetalert2";

export default function AdminProductCard({product}) {

	const { name, description, price, _id, isActive } = product;
	const navigate = useNavigate();

	const deactivateProduct = (e, productId) => {
		Swal.fire({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, Deactivate it!'
		}).then((result) => {
		  if (result.isConfirmed) {
		  	fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
				method: "PATCH",
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {

				if(data) {
				    Swal.fire(
				      'Deactivated!',
				      'Your product has been deactivated.',
				      'success'
				    )
				    window.location.reload(true);
					navigate("/admin")

				} else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					})
				}

			})
		  }
		})
	};

	const activateProduct = (e, productId) => {
		Swal.fire({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, Activate it!'
		}).then((result) => {
		  if (result.isConfirmed) {
		  	fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/activate`, {
				method: "PATCH",
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {

				if(data) {
				    Swal.fire(
				      'Activated!',
				      'Your product has been activated.',
				      'success'
				    )
				    window.location.reload(true);
					navigate("/admin")

				} else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					})
				}

			})
		  }
		})
	};

	return (
		
		<Col className='my-2' xs={12} md={4}>
			<Card className="cardHighlight p-0">
				<Card.Body>
					<Card.Title><h4>{name}</h4></Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>${price}</Card.Text>
					<Row className="mt-4" >
				        <Col>
				          <Button className="m-1" variant="danger" as={Link} to={`/products/${_id}/update`}>Update</Button>
				          {
				          	(isActive)?
					          <Button variant="danger" onClick = {(e) => deactivateProduct(e,_id)}>Deactivate Product</Button>
					          :
					          <Button variant="success" onClick = {(e) => activateProduct(e,_id)}>Activate Product</Button>
				          }
				        </Col>
			        </Row>
				</Card.Body>
			</Card>
		</Col>
	)
}
