import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {
	const arr = new Array(1);
	arr.fill("hii")
	return (
		    <Row className="mt-3 mb-3">
			   {arr.map((el, idx) => <Col key={idx} xs={12} md={8}>
			            <Card className="cardHighlight p-3">
			                <Card.Body>
			                    <Card.Title>
			                        <h2>Affordable and Feature-Packed Smartphones</h2>
			                    </Card.Title>
			                    <Card.Text>
			                    	Don't compromise on quality or features. Explore our collection of affordable smartphones that deliver impressive performance, exceptional cameras, and long-lasting battery life.
			                    </Card.Text>
			                    <Card.Text>
			                    	Capture every moment in exceptional detail with our camera-centric smartphones. Discover phones designed to help you express your creativity and share memories like never before.
			                    </Card.Text>
			                </Card.Body>
			            </Card>
			        </Col>
			    )}
		    </Row>
		)
}

