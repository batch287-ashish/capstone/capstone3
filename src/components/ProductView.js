import {useState, useContext, useEffect, useMemo} from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import {useParams, Link, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';
import QuantityButton from './quantityButton';
import Swal from 'sweetalert2';

export default function ProductView() {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	const {productId} = useParams();

	const [order, setOrder] = useState({
		name: "",
		description: "",
		quantity: 1,
		price: 0
	});


 	const swalWithBootstrapButtons = Swal.mixin({
	  customClass: {
	    confirmButton: 'btn btn-success',
	    cancelButton: 'btn btn-danger'
	  },
	  buttonsStyling: false
	})

	const handleSubmit = (e,productId) => {
		e.preventDefault();

		if(user.isAdmin === true){
			Swal.fire({
				title: "Not able to order",
				icon: "error",
				text: "Admin can't place orders :("
			})
		}

		else{
			swalWithBootstrapButtons.fire({
			  title: 'Are you sure?',
			  text: "You won't be able to revert this!",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonText: 'Place order!',
			  cancelButtonText: 'Cancel!',
			  reverseButtons: true
			}).then((result) => {
			  if (result.isConfirmed) {
			     	fetch(`${process.env.REACT_APP_API_URL}/users/checkOut`, {
						method: "POST",
						headers: {
							'Content-Type': 'application/json',
							Authorization: `Bearer ${localStorage.getItem('token')}`
						},
						body: JSON.stringify({
							productId: productId,
							quantity: order.quantity
						})
					})
					.then(res => res.json())
					.then(data => {

						if(data) {
							Swal.fire({
								title: "Successfully ordered",
								icon: "success",
								text: "You have successfully ordered the product."
							})

							navigate("/products")

						} else {
							Swal.fire({
								title: "Something went wrong",
								icon: "error",
								text: "Please try again."
							})
						}

					})
			  } else if (
			    result.dismiss === Swal.DismissReason.cancel
			  ) {
			    swalWithBootstrapButtons.fire(
			      'Cancelled',
			      'Your order is cancelled :(',
			      'error'
			    )
			  }
			})
	    }
	};

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(({name, description, price}) => {
			setOrder({
				...order,
				name,
				description,
				price
			})
		})
	}, [productId]);

	const totalPrice = useMemo(() => {
		return order.price * order.quantity;
 	}, [order.price, order.quantity]);

	return (

		<Container>
			<Row>
				<Col lg={{span: 6, offset:3}} >
					<Card>
					      <Card.Body className="text-center">
					        <Card.Title>{order.name}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{order.description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>$ {order.price}</Card.Text>
					        <Card.Subtitle>Quantity:</Card.Subtitle>
					        <QuantityButton setOrder={setOrder} order={order}/>
					        <div className="mb-3">
	    						TotalPrice: <strong>$ {totalPrice}</strong>
							</div>
					        {
					        	(user.id !== null) ?
					        		<Button variant="danger" onClick={(e) => handleSubmit(e,productId)}>Order</Button>
					        		:
					        		<Button className="btn btn-danger" as={Link} to="/login">Log in to Order</Button>
					        }

					      </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}
