import React, { useState, useMemo } from 'react';
import { Button, Form } from 'react-bootstrap';
import '../style.scss';

export default function QuantityButton({order, setOrder}) {
  const incremet = () => setOrder({...order, quantity: order.quantity + 1});
  const decrement = () => setOrder({...order, quantity: order.quantity - 1});

  return (
    <div className="container my-4">
      <div className="quantity d-inline-flex align-items-center justify-content-between position-relative border rounded-pill">
        <Button
          variant="danger"
          className="rounded-circle p-0"
          onClick={decrement}
          disabled={order.quantity <= 1}
        >
          -
        </Button>
        <Form.Control
          type="number"
          name="quantity"
          value={order.quantity}
          onChange={(e) => {}}
          onBlur={() => {}}
        />
        <Button
          variant="success"
          className="rounded-circle p-0"
          onClick={incremet}
        >
          +
        </Button>
      </div>
    </div>
  );
}