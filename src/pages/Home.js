import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {

	const data = {
		title: "E-shopping site",
		content: "Best Smartphones available for everyone and to everywhere!",
		destination: "/login",
		label: "Order Now!"
	};

	return(
		<>
			<Banner data={data} />
			<Highlights />
		</>
	)
};