import React from 'react';
import { useContext } from 'react';
import AdminProducts from './AdminProducts';
import UserContext from '../UserContext';
import { Link } from 'react-router-dom';
import {useNavigate} from 'react-router-dom';
import { Container, Row, Col, Button } from 'react-bootstrap';

export default function Admin() {

	const { user } = useContext(UserContext);
	const navigate = useNavigate();

	if(user.isAdmin === true){
		return(
			<>
				<Container className="d-flex justify-content-center">
			      <div>
			        <h2 className="text-center">Admin Dashboard</h2>
			        <Container className="d-flex justify-content-center">
				        <Col xs={6}>
				          <Button variant="danger" as={Link} to="/productForm">Add New Product</Button>
				        </Col>
				        <Col xs={8}>
				          <Button variant="secondary">Show User Products</Button>
				        </Col>
			        </Container>
			      </div>
			    </Container>
			    <Row className="mt-3 mb-3">
			    	<AdminProducts />
			    </Row>
			</>
	    )
	}
	else{
		navigate("/login")
	}
};