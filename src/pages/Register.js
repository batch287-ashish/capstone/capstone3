import { useState, useEffect, useContext } from 'react';
import { Col, Button, Row, Container, Card, Form } from 'react-bootstrap';
import { useNavigate,Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register() {

    const {user} = useContext(UserContext);

    //an object with methods to redirect the user
    const navigate = useNavigate();

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [isActive, setIsActive] = useState('');

    // Function to simulate user registration
    function registerUser(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        //fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {

            if(data === true){

                Swal.fire({
                    title: 'Duplicate email found',
                    icon: 'error',
                    text: 'Please provide a different email.'   
                });

            } else {
                fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        email: email,
                        password: password1
                    })
                })
                .then(res => res.json())
                .then(data => {

                    if(data === true){
                        setEmail('');
                        setPassword1('');
                        setPassword2('');

                        Swal.fire({
                            title: 'Registration successful',
                            icon: 'success',
                            text: 'Welcome to E-Shop!'
                        });

                        // Allows us to redirect the user to the login page after registering for an account
                        navigate("/login");

                    } else {

                        Swal.fire({
                            title: 'Something wrong',
                            icon: 'error',
                            text: 'Please try again.'   
                        });

                    };

                })
            };

        })

    }

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [email, password1, password2]);

    return (
        (user.id !== null) ?
            <Navigate to="/products" />
        :
          (
            <div>
              <Container>
                <Row className="vh-100 d-flex justify-content-center align-items-center">
                  <Col md={8} lg={6} xs={12}>
                    <Card className="px-4">
                      <Card.Body>
                        <div className="mb-3 mt-md-4">
                          <h2 className="fw-bold mb-2 text-center text-uppercase ">
                            Register
                          </h2>
                          <div className="mb-3">
                            <Form onSubmit={(e) => registerUser(e)}>
                              <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label className="text-center">
                                  Email address
                                </Form.Label>
                                <Form.Control 
                                  type="email" 
                                  placeholder="Enter email"
                                  value={email} 
                                  onChange={e => setEmail(e.target.value)}
                                  required
                                />
                              </Form.Group>

                              <Form.Group
                                className="mb-3"
                                controlId="formBasicPassword"
                              >
                                <Form.Label>Password</Form.Label>
                                <Form.Control 
                                  type="password" 
                                  placeholder="Password"
                                  value={password1} 
                                  onChange={e => setPassword1(e.target.value)}
                                  required
                                />
                              </Form.Group>
                              <Form.Group
                                className="mb-3"
                                controlId="formBasicPassword"
                              >
                                <Form.Label>Confirm Password</Form.Label>
                                <Form.Control 
                                  type="password" 
                                  placeholder="Verify Password"
                                  value={password2} 
                                  onChange={e => setPassword2(e.target.value)}
                                  required
                                />
                              </Form.Group>
                              <Form.Group
                                className="mb-3"
                                controlId="formBasicCheckbox"
                              ></Form.Group>
                              <div className="d-grid">
                                {
                                  (isActive)?(
                                    <Button variant="danger" type="submit">
                                      Please enter your registration details..
                                    </Button>
                                  ):(
                                    <Button variant="danger" type="submit" disabled>
                                      Please enter your registration details..
                                    </Button>
                                  )
                                }
                              </div>
                            </Form>
                            <div className="mt-3">
                              <p className="mb-0  text-center">
                                Already have an account??{' '}
                                <a href="/login" className="text-danger fw-bold">
                                  Log In
                                </a>
                              </p>
                            </div>
                          </div>
                        </div>
                      </Card.Body>
                    </Card>
                  </Col>
                </Row>
              </Container>
            </div>
      )
   )
}